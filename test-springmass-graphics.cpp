/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{

private:
	Figure figure;

public:
	SpringMassDrawable() : figure("Bouncing ball with springs")
	{
		figure.addDrawable(this);
	}

	void draw() 
	{
		for (int i = 0; i < (int)masses.size(); i++)
		{
			figure.drawCircle(masses[i].getPosition().x, masses[i].getPosition().y, masses[i].getRadius());
		}

		for (int i = 0; i < (int)springs.size(); i++)
		{
			figure.drawLine(springs[i].getMass1()->getPosition().x,
							springs[i].getMass1()->getPosition().y,
							springs[i].getMass2()->getPosition().x,
							springs[i].getMass2()->getPosition().y, 1.0 );
		}
	}

	void display()
	{
		figure.update();
	}

} ;

int main(int argc, char** argv)
{
  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.1 ;
  const double dumping = 0.01 ;
  const double stiffness = 1.0 ;
  const int gridX = 10 ;
  const int gridY = 10 ;

  glutInit(&argc,argv) ;

  SpringMassDrawable springmass ;

  for (int i = 0; i < gridX; i++)
  {
  	for (int j = 0; j < gridY; j++)
  	{
  		Mass m(Vector2(-.5 + (j * 0.1) + (0.05 * i), -.5 + (i * .1) + (0.01 * j)), Vector2(), mass, radius);
  		springmass.addMass(m);
  	}
  }

  for (int i = 0; i < (gridX * gridY) - 1; i++)
  {
  	if ((i + 1) % gridX != 0)
  		springmass.addSpring(i, i + 1, naturalLength , stiffness, dumping);
  	if (i + gridY < gridX * gridY)
  		springmass.addSpring(i, i + gridY, naturalLength, stiffness, dumping);
  }


  run(&springmass, 1/120.0) ;
}
